# 代码编辑工具相关配置

### 目录
1. [VScode](#vscode)
    - [VScode环境配置](#vscode环境配置)
    - [VScode插件推荐](#vscode插件推荐)
2. [Sublime](#sublime)   
    - [sublime环境配置](#sublime环境配置)
    - [sublime插件推荐](#sublime插件推荐)

---
## VScode
### VScode环境配置
- Encoding -> utf8    
- 

---
### VScode插件推荐
- Anaconda Extension Pack    
- C/C++    
- Chinese (Simplified) Language Pack for Visual Studio Code    
- Markdown All in One   
- Markdown Preview Enhanced   
- Markdown Shortcuts    
- Material Icon Theme    
- Python    
- Sublime Text Keymap and Settings Importer    
- Visual Studio Code Tools for AI    
- vscode-icons    


---
## Sublime
### sublime环境配置
```json
{
    "font_size": 11,
    "translate_tabs_to_spaces": true
}
```



---
### sublime插件推荐
- [Package Control](https://packagecontrol.io/installation)
The simplest method of installation is through the Sublime Text console. The console is accessed via the **ctrl+`** shortcut or the **View > Show Console menu**. Once open, paste the appropriate Python code for your version of Sublime Text into the console.    
**然后输入以下命令：**    
import urllib.request,os,hashlib; h = '6f4c264a24d933ce70df5dedcf1dcaee' + 'ebe013ee18cced0ef93d5f746d80ef60'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)
    
- convertToUTF8 : 解决中文显示乱码问题；      
- sublimerge : sublime text 官方代码对比工具；   
- 